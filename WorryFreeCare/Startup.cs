﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(WorryFreeCare.Startup))]
namespace WorryFreeCare
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
